package parent;

public class Person {
    private String name;
    private String address;
    final String country = "Indonesia";

    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Person() {
        super();
    }

    public void greeting(){
        System.out.println("Hello , Myname is " + name +".");
        System.out.println("I, come from "  + address + ".");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

package child;

import parent.Person;

public class Teacher extends Person {
    private String subject;

    public Teacher() {
    }

    public Teacher(String name, String address, String subject) {
        super(name, address);
        this.subject = subject;
    }

    public void teaching(){
        System.out.println("i can teach "+ subject +", So anyone who wants to learn can talk to me");
    }

    //@Override
    public void greeting() {
        //super.greeting();
        System.out.println("Hello my name is " + getName() );
        System.out.println("I come from "+ getAddress());
        System.out.println("My job is a "+subject+" teacher.");
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}

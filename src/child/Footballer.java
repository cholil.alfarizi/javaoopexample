package child;

import parent.Person;

public class Footballer extends Person {
    private String club;

    public Footballer() {
        super();
    }

    public Footballer(String name, String address, String club) {
        super(name, address);
        this.club = club;
    }

    public void juggling(){
        System.out.println("I can do ball juggling");
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    @Override
    public void greeting() {
        super.greeting();
        System.out.println("I'm a football player from "+ club);
    }
}

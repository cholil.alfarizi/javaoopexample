package child;

import parent.Person;

public class Programmer extends Person {
    private String technology;

    public Programmer() {
        super();
    }

    public Programmer(String name, String address, String technology) {
        super(name, address);
        this.technology = technology;
    }

    public void hacking(){
        System.out.println("I can hacking a website");
    }

    public void coding(){
        System.out.println("I can create an application using technology : "+ technology +".");
    }

    @Override
    public void greeting() {
        super.greeting(); //memanggil method greeting dari parent class
        System.out.println("My job is a "+technology+" programmer.");
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }
}

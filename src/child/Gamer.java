package child;

import parent.Person;

public class Gamer extends Person {
    private String game;

    public Gamer() {
        super();
    }

    public Gamer(String name, String address, String game) {
        super(name, address);
        this.game = game;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    @Override
    public void greeting() {
        super.greeting();
        System.out.println("I'm a professional gamer of "+game);
    }
}

package child;

import parent.Person;

public class Doctor extends Person {
    private String specialist;

    public Doctor() {
        super();
    }

    public Doctor(String name, String address, String specialist) {
        super(name, address);
        this.specialist = specialist;
    }

    public void surgery(){
        System.out.printf("I can surgery operation Patients");
    }

    @Override
    public void greeting() {
        super.greeting();
        System.out.println("My job is a "+specialist+" doctor.");
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }
}

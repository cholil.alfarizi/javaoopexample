import child.*;
import parent.Person;

public class MainApp {
    public static void main(String[]args){
        Person person1 = new Person();
        person1.setName("hendar");
        person1.setAddress("Garut");

        Teacher teacher1 = new Teacher();
        teacher1.setName("Budi");
        teacher1.setAddress("bandung");
        teacher1.setSubject("matemetika");

        //object doctor yang menggunakan constuctor default
        Doctor doctor1 = new Doctor();
        doctor1.setName("Elis");
        doctor1.setAddress("Jakarta");
        doctor1.setSpecialist("Dentis");

        Footballer footballer1 = new Footballer();
        footballer1.setName("Atep");
        footballer1.setClub("Persib Bandung");
        footballer1.setAddress("Bandung");

        Gamer gamer1 = new Gamer();
        gamer1.setName("Kratos");
        gamer1.setGame("Dota 2");
        gamer1.setAddress("Cimahi");

        //object doctor yang menggunakan constructor berparameter
        Doctor doctor2 = new Doctor("Joko", "Tegal", "Cardiologist");

        Programmer programmer1 = new Programmer();
        programmer1.setName("rizki");
        programmer1.setAddress("Surabaya");
        programmer1.setTechnology("javascript");

        //Polymorphism
        Programmer person4 = new Programmer("Rizky","Bandung",".Net Core");
        person4.greeting();
        System.out.println(person4.getTechnology());
        System.out.println();

        Person person2 = new Teacher("Joko","Tegal","Matematika");
        Person person3 = new Doctor("Eko", "Surabaya","Pedistrician");

        sayHello(person4);
        sayHello(person2);
        sayHello(person3);
        System.out.println("??????????????????????????????????????");

        person1.greeting();
        System.out.println();
        teacher1.greeting();
        System.out.println();
        doctor1.greeting();
        System.out.println();
        programmer1.greeting();
        System.out.println();
        doctor2.greeting();
        System.out.println();
        footballer1.greeting();
        System.out.println();
        gamer1.greeting();



    }
    static void sayHello(Person person){
        String message;
        if (person instanceof Programmer){
            Programmer programmer = (Programmer) person;
            message = "Hello, "+ programmer.getName() + ". Seorang Programmer " + programmer.getTechnology() + ".";
        }else if (person instanceof Teacher){
            Teacher teacher = (Teacher) person;
            message = "Hello, "+ teacher.getName() + ". Seorang Guru " + teacher.getSubject() + ".";
        }else if (person instanceof Doctor){
            Doctor doctor = (Doctor) person;
            message = "Hello, "+ doctor.getName() + ". Seorang Dokter " + doctor.getSpecialist() + ".";
        }else {
            message = "Hello, "+ person.getName() + ".";
        }
        System.out.println(message);
    }
}
